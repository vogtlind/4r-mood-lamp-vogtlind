
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHTesp.h"
#include <Adafruit_NeoPixel.h>

//OTA
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
AsyncWebServer server(80);
String ipadress;
//OTA ENDE

DHTesp dht;
// Update these with values suitable for your network.

#define PIN D4  // PIN, na kterém je připojen váš Neopixel
#define NUMPIXELS 115  // Počet pixelů na vašem Neopixel pásku

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

bool runSemafor = false;  // Globální proměnná pro ovládání efektu semaforu
//pripojeni k wifi
const char* ssid = "Xiaomi 11T";
const char* password = "sutobus2007";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;


void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

   //OTA
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! This is a sample response.");
  });

  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");
  //OTA ENDE
}

bool runRainbow = false;  // Globální proměnná pro ovládání efektu duhy

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

     if(strcmp(topic,"4h/dan/menic")==0){
    
    setStripRGB(payload, length);
  }


  // Předpokládáme, že zpráva '0' znamená vypnout a '1' znamená spustit duhu
  if(strcmp(topic,"4h/dan/efekty")==0){    
    if((char)payload[0] == '0'){    //OFF
      runRainbow = false;  // Zastaví efekt duhy
      vypnuto();
    } else if((char)payload[0] == '1'){    //Rainbow
      runRainbow = true;  // Spustí efekt duhy
      rainbow();
    }
    
    if((char)payload[0] == '2'){    //Bila
      svitBila();
    }
    if((char)payload[0] == '3'){    //Bila
      svitOranzova();
    }
     if(strcmp(topic,"4h/dan/efekty")==0){    
    if((char)payload[0] == '0'){    //OFF
      runSemafor = false;  // Zastaví efekt semaforu
      vypnuto();
    } else if((char)payload[0] == '4'){    //Semafor
      runSemafor = true;  // Spustí efekt semaforu
      semafor();
    }
  }
  }
  
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);

    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("4h/dan/test", "hello world");
      // ... and resubscribe
      client.subscribe("4h/dan/efekty");
      client.subscribe("4h/dan/menic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  dht.setup(D5, DHTesp::DHT11);
  pixels.begin();  // Inicializace Neopixel
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  // Testování funkcí
  svitBila();  // Spustí efekt duhy
  delay(5000);  // Počká 5 sekund
  vypnuto();  // Vypne LED
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  //------------------------
  
 
  unsigned long now2 = millis();
  if(now2 - lastMsg > 2000){
    lastMsg = now2;
    TeplotaAVlhkost();
  }
   //------------

  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("4h/dan/test", msg);
  }
}



//-----------------------------------------------------------

void TeplotaAVlhkost(){
    float h = dht.getHumidity();
    float t = dht.getTemperature();
 
    Serial.print("{\"humidity\": ");
    Serial.print(h);
    Serial.print(", \"temp\": ");
    Serial.print(t);
    Serial.print("}\n");
 
    delay(2000);
  
   //t.publish("", tepStr.c_str());
    
    String vlhStr = String(h, 2);
    client.publish("4h/dan/humidity", vlhStr.c_str());
    String tepStr = String(t, 2);
    client.publish("4h/dan/teplota", tepStr.c_str());

   
  }


  
void rainbow(){
    for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {  
    for(int i=0; i<pixels.numPixels(); i++) {  // Pro každý pixel...
      if (!runRainbow) return;  // Přeruší efekt duhy, pokud runRainbow je false
      int pixelHue = firstPixelHue + (i * 65536L / pixels.numPixels());
      setAll(pixels.gamma32(pixels.ColorHSV(pixelHue)));
      client.loop();  // Přidáno: zpracování zpráv MQTT během efektu duhy
      delay(10);  
    }
   }    
}


void svitBila() {
  uint32_t bila = pixels.Color(255, 255, 255);  // Vytvoří bílou barvu
  for(int i=0; i<NUMPIXELS; i++) {  // Pro každý pixel...
    pixels.setPixelColor(i, bila);  // Nastaví pixel na bílou
  }
  pixels.show();  // Aktualizuje pixely
}

  // Funkce pro nastavení všech pixelů na danou barvu
  void setAll(uint32_t color) {
    for(int i=0; i<NUMPIXELS; i++) {  // Pro každý pixel...
      pixels.setPixelColor(i, color);  // Nastaví barvu pixelu
    }
    pixels.show();  // Aktualizuje pixely
  }

 // Funkce pro vypnutí všech pixelů
void vypnuto() {
  for(int i=0; i<NUMPIXELS; i++) {  // Pro každý pixel...
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));  // Vypne pixel
  }
  pixels.show();  // Aktualizuje pixely
}


void nastavRGB (byte r, byte g, byte b, int cislo) {
  uint32_t barva;
  barva = pixels.Color(r, g, b);
  pixels.setPixelColor(cislo - 1, barva);
  pixels.show();
}

void setStripRGB(byte* payload, unsigned int length){                       //RGB
  payload[length] = '\0'; // Přidání ukončovací nuly pro vytvoření řetězce
    String payloadStr = String((char*)payload);

    // Hledání hodnot r, g, b ve formátu "rgb(x, y, z)"
    int r, g, b;
    if (sscanf(payloadStr.c_str(), "rgb(%d, %d, %d)", &r, &g, &b) == 3) {
      Serial.print("r = ");
      Serial.print(r);
      Serial.print(" | g = ");
      Serial.print(g);
      Serial.print(" | b = ");
      Serial.println(b);
      for(int i=1; i<=115; i++){
        nastavRGB(r,g,b,i);
      }
    } else {
      Serial.println("Invalid RGB format");
    }
}

void svitOranzova() {
  // Definujte příjemnou oranžovou barvu
  int red = 255;
  int green = 153;
  int blue = 0;

  for(int i=0; i<NUMPIXELS; i++) {  
    pixels.setPixelColor(i, pixels.Color(red, green, blue));  
  }
  pixels.show();
}

void semafor() {
  int cervena = NUMPIXELS / 3;  // Poslední třetina LED pásku
  int zluta = 2 * NUMPIXELS / 3;  // Druhá třetina LED pásku

  // Nastaví poslední třetinu LED pásku na červenou
  for(int i = zluta; i < NUMPIXELS; i++) {
    if (!runSemafor) return;  // Přeruší efekt semaforu, pokud runSemafor je false
    pixels.setPixelColor(i, pixels.Color(255, 0, 0));  // Červená barva
  }
  pixels.show();
  delay(1000);  // Počká sekundu

  // Přidá druhou třetinu LED pásku a nastaví ji na žlutou
  for(int i = cervena; i < zluta; i++) {
    if (!runSemafor) return;  // Přeruší efekt semaforu, pokud runSemafor je false
    pixels.setPixelColor(i, pixels.Color(255, 255, 0));  // Žlutá barva
  }
  pixels.show();
  delay(1000);  // Počká sekundu

  // Rozsvítí celý LED pásek zeleně
  for(int i = 0; i < NUMPIXELS; i++) {
    if (!runSemafor) return;  // Přeruší efekt semaforu, pokud runSemafor je false
    pixels.setPixelColor(i, pixels.Color(0, 255, 0));  // Zelená barva
  }
  pixels.show();
}

