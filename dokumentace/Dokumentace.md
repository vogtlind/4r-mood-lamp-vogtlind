
## MoodLamp projekt
**Cíl:** 
* Vytvořit mood lampu, která se dá ovládat přes webovou stránku
**Potřebné součástky**
* WeMos D1 mini Pro
* TP4056 USB Type C
* Switch
* 18650 Li-On baterie
* WS2812B LED pásek 
* 1000uF Kapacitor
* DHT11 senzor 
* drátky
* vytisknuté tělo MoodLampy z 3D tiskárny

**Návod**
* 1. vytisknout s 3D tiskárnou těla Mood Lampy
* 2. testujeme součástky, jestli fungují
* 3. jelikož nám všechny součástky fungují, můžene pokračovat s nakresem zapojení
* 4. dále testujeme jednotlivé součastky jednoduchým kódem
* 5. nastal čas součástky spájet a otestovat jestli je všechno správně spájený, nahrajeme testovací kód

**Vytvoření rozhraní pro ovládání MoodLampy**
* 1. Rozhraní vytvoříme v Node-Redu pro jednoduché ovládání MoodLampy připojené do naší sítě
* 2. V Node-Redu musíme vytvořit propojení s WeMosem pomocí HiveM MQTT serveru
* 3. Nahráli jsme také OTA pro odesílaní aktualizace softwaru přes WiFi
* 4. V Node-Redu jsme vytvořili funkce, aby projekt splňoval požadavky vyučujícího
* 5. Poté v UI v Node-Redu můžeme ovládat všechny efekty Mood Lampy a naměřené hodnoty

**Výsledek**
* Projekt mi zabral na zhotovení 12 hodin 
* Během plnění projektu nenastali žadné závažné komplikace
* Výsledek mé práce je hotový projekt, který je připravený k prezentaci 