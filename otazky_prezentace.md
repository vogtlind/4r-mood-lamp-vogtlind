# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 12 Hodin                        |
| odkud jsem čerpal inspiraci                   |       https://www.instructables.com/RGB-Lamp-WiFi/                    |
| odkaz na video                                | https://www.youtube.com/watch?v=AtjgQkSEeNY&ab_channel=dani3l302                     |
| jak se mi to podařilo rozplánovat             |   naplanoval jsem si to tak, abych to časově stihal do odevzdání    |
| proč jsem zvolil tento design                 | Protože je nejvíc praktický a jelikoz to má byt lampa tak to taky musi nejak vypadat :D |
| zapojení                                      | https://gitlab.spseplzen.cz/vogtlind/4r-mood-lamp-vogtlind/-/blob/main/dokumentace/schema/mood_lampa_fritzing_bb.png  |
| z jakých součástí se zapojení skládá          | Wemos D1 Mini Pro, DHT11, 18650 Li-On baterie, WS2812B pásek, Drátky, TP4056  |
| realizace                                     | https://gitlab.spseplzen.cz/vogtlind/4r-mood-lamp-vogtlind/-/blob/main/dokumentace/fotky/1709705388612.jpg |
| UI                                            | https://gitlab.spseplzen.cz/vogtlind/4r-mood-lamp-vogtlind/-/blob/main/dokumentace/fotky/Screenshot_2024-03-06_055723.png   |
| co se mi povedlo                              |   zkompletovat celý produkt na první pokus          |
| co se mi nepovedlo/příště bych udělal/a jinak |     mě se vsechno povedlo tak jak jsem to mel naplánovaný     |
| zhodnocení celé tvorby (návrh známky)         |  Krásná práce, takže 1                    |
